# terraria-server

## Ansible native examples

```yaml
# Loop over hostvars
- name: Loop over hostvars using "query"
  debug:
    msg: "IP is: {{ hostvars[item]['ip'] }}"
  loop: "{{ query('inventory_hostnames', 'terraia_servers') }}"

# Generate new range of integers, based on {{ port }} and  {{ count }} from group_vars
- name: Set "ports" fact to be used for firewall stuff
  vars:
    ports: []
  set_fact:
    ports: "{{ ports }} + [ '{{ port + item }}' ]"
  changed_when: false
  loop: "{{ range(1, count + 1) | list }}"

# Create dynamic host_group, use the generated var from previous step
- name: Add the Terraria servers to a host group
  add_host:
    name: "{{ inventory_hostname }}"
    groups: servers
    ip: "{{ ansible_default_ipv4.address }}"
    ports: "{{ ports }}"
  changed_when: false

# Use dynamic host_vars from the dynamic host group from previous step to do stuff, in a nested loop
- name: Print output fromt the nested loop
  debug: "item[0]: {{ item[0] }}, item[1]: {{ item[1] }}"
  with_nested:
    - "{{ groups['servers'] | map('extract',hostvars,'ip') | list }}"
    - "{{ groups['servers'] | map('extract',hostvars,'ports') | list | join(',') }}"
```
